<?php

namespace App\Http\Controllers;

use App\Models\AceiteDeCookies;
use App\Models\QuemSomos;
use App\Models\Clientes;
use Illuminate\Http\Request;

class QuemSomosController extends Controller
{
    public function index()
    {
        $quem_somos = QuemSomos::first();
        $clientes = Clientes::ordenados()->get();

        return view('site.quem_somos', compact('quem_somos','clientes'));
    }

}
