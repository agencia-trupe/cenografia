<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClientesRequest;
use App\Models\Clientes;
use Illuminate\Http\Request;

class ClientesController extends Controller
{
    public function index()
    {
        $clientes = Clientes::ordenados()->get();

        return view('painel.clientes.index', compact('clientes'));
    }

    public function create()
    {
        return view('painel.clientes.create');
    }

    public function store(ClientesRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Clientes::upload_imagem();

            Clientes::create($input);

            return redirect()->route('clientes.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Clientes $cliente)
    {
        return view('painel.clientes.edit', compact('cliente'));
    }

    public function update(ClientesRequest $request, Clientes $cliente)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Clientes::upload_imagem();

            $cliente->update($input);

            return redirect()->route('clientes.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Clientes $cliente)
    {
        try {
            $cliente->delete();

            return redirect()->route('clientes.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }

}
