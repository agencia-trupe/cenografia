@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>CLIENTES |</small> Editar Cliente</h2>
</legend>

{!! Form::model($cliente, [
'route' => ['clientes.update', $cliente->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.clientes.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection