@include('painel.layout.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group mt-3 mb-3">
    {!! Form::label('video', 'URL de incorporação do Vídeo (exemplo: https://www.youtube.com/embed/dvAbtpCxbCE)') !!}
    {!! Form::text('video', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success mt-3 mb-3']) !!}

<a href="{{ route('projetos.videos.index', $projeto->id) }}" class="btn btn-default btn-light">Voltar</a>
