@extends('site.layout.template')

@section('content')

<main class="politica-de-privacidade">
    <div class="center">
        <h2 class="titulo">Política de Privacidade</h2>
        <div class="texto">{!! $politica->texto_pt !!}</div>
    </div>
</main>

@endsection