@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>Projetos Imagem |</small> Gerenciar Imagem</h2>
</legend>

{!! Form::model($imagen, [
'route' => ['projetos.imagens.update', ['projeto' => $projeto->id, 'imagen' => $imagen->id]],
'method' => 'patch',
'files' => true])
!!}

@include('painel.projetos.imagens.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection