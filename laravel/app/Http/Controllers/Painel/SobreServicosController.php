<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\SobreServicosRequest;
use App\Models\SobreServicos;
use Illuminate\Http\Request;

class SobreServicosController extends Controller
{
    public function index()
    {
        $sobre = SobreServicos::first();

        return view('painel.sobre-servicos.edit', compact('sobre'));
    }

    public function update(SobreServicosRequest $request, SobreServicos $sobre_servico)
    {
        try {
            $input = $request->all();

            $sobre_servico->update($input);

            return redirect()->route('sobre-servicos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
