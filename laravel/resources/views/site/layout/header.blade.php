<header>

    <div class="center">

    <nav id="nav-desktop">
            <a href="{{ route('home') }}" @if(Tools::routeIs('home')) style="color: #fff" @endif>
                <div class="menu_cabec">
                    home
                </div>
            </a>
            <a href="{{ route('somos') }}">
                <div @if(Tools::routeIs('home')) style="color: #fff"  class="menu_cabec2" @elseif(Tools::routeIs('somos*')) class="menu_cabec2 active" @else class="menu_cabec2"
                @endif>
                quem somos
                </div>
            </a>
           
            <a href="{{ route('servicos') }}">
                <div @if(Tools::routeIs('home')) style="color: #fff"  class="menu_cabec3" @elseif(Tools::routeIs('servicos*')) class="menu_cabec3 active" @else class="menu_cabec3"
                @endif>
                nossos serviços
                </div>
            </a>

            <a href="{{ route('projetos') }}">
                <div @if(Tools::routeIs('home')) style="color: #fff"  class="menu_cabec4" @elseif(Tools::routeIs('projetos*')) class="menu_cabec4 active" @else class="menu_cabec4"
                @endif>
                projetos realizados
                </div>
            </a>

            <a href="{{ route('clippings') }}">
                <div @if(Tools::routeIs('home')) style="color: #fff"  class="menu_cabec5" @elseif(Tools::routeIs('clippings*')) class="menu_cabec5 active" @else class="menu_cabec5"
                @endif>
                clipping
                </div>
            </a>

            <a href="{{ route('contato') }}">
                <div @if(Tools::routeIs('home')) style="color: #fff"  class="menu_cabec6" @elseif(Tools::routeIs('contato*')) class="menu_cabec6 active" @else class="menu_cabec6"
                @endif>
                contato
                </div>
            </a>

            @if($contato->instagram)
                <a href="{{$contato->instagram}}" @if(Tools::routeIs('home')) class=""  @elseif(Tools::routeIs('*')) class="instagreen" 
                @endif><img src="{{ asset('assets/img/layout/ico-instagram-branco.svg') }}" alt=""></a>
            @endif
           
        </nav>

        <button id="mobile-toggle" type="button" role="button">
            <span @if(Tools::routeIs('home')) class="lines2" @else class="lines" @endif></span>
        </button>

    </div>

    <nav id="nav-mobile">
        <div class="center-header">
        
            <a href="{{ route('home') }}" @if(Tools::routeIs('home')) class="active" @endif>
                home
            </a>
            <a href="{{ route('somos') }}" @if(Tools::routeIs('quem_somos')) class="active" @endif>
            quem somos
            </a>
            <a href="{{ route('servicos') }}" @if(Tools::routeIs('servicos*')) class="active" @endif>
            nossos serviços
            </a>
            <a href="{{ route('projetos') }}" @if(Tools::routeIs('clippings')) class="active" @endif>
            projetos realizados
            </a>   
            <a href="{{ route('clippings') }}" @if(Tools::routeIs('contato')) class="active" @endif>
            clipping
            </a>
            <a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>
            contato
            </a>

            @if($contato->instagram)
                <a href="{{$contato->instagram}}" class="link-nav"><img src="{{ asset('assets/img/layout/ico-instagram-branco.svg') }}" alt=""></a>
            @endif
        </div>
    </nav>

</header>