<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\QuemSomosRequest;
use App\Models\QuemSomos;
use Illuminate\Http\Request;

class QuemSomosController extends Controller
{
    public function index()
    {
        $quem_somos = QuemSomos::first();

        return view('painel.quem-somos.edit', compact('quem_somos'));
    }

    public function update(QuemSomosRequest $request, QuemSomos $quem_somo)
    {
        try {
            $input = $request->all();

            $quem_somo->update($input);

            return redirect()->route('quem-somos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }
}
