<footer>

    <div class="center">

        <a href="{{ route('home') }}" class="link-home">
            <img src="{{ asset('assets/img/layout/marca-cenografia-sustentavel.svg')}}" alt="{{ $config->title }}" class="img-logo">
        </a>

        <article class="menu">
            <a href="{{ route('home') }}" class="link-footer @if(Tools::routeIs('home*')) active @endif">home</a>
            <a href="{{ route('somos') }}" class="link-footer @if(Tools::routeIs('home*')) active @endif">quem somos</a>
            <a href="{{ route('servicos') }}" class="link-footer @if(Tools::routeIs('home*')) active @endif">nossos serviços</a>
            <a href="{{ route('projetos') }}" class="link-footer @if(Tools::routeIs('home*')) active @endif">projetos realizados</a>
            <a href="{{ route('clippings') }}" class="link-footer @if(Tools::routeIs('home*')) active @endif">clipping</a>
            <a href="{{ route('contato') }}" class="link-footer @if(Tools::routeIs('home*')) active @endif">contato</a>
        </article>

        <article class="categorias">
            @foreach($servicos as $servico)
            <a href="{{ route('servicos.slug', ['#'.$servico->slug])}}">{{$servico->titulo}}</a>
            @endforeach
        </article>

        <article class="informacoes">

            @php
            $telefone = str_replace(array(" ", "-"), '', $contato->telefone);
            $celular = str_replace(array(" ", "-"), '', $contato->celular);
            @endphp

            @if($contato->telefone)
            <a href="https://api.whatsapp.com/send?phone=55{!!$celular!!}&text=ol%C3%A1%20tudo%20bem">
                <div class="info_card">
                    <img src="{{ asset('assets/img/layout/ico-whatsapp.svg')}}" alt="">
                    <p>+55 {{$contato->telefone}}</p>
                </div>
            </a>
            @endif
            @if($contato->celular)
            <a href="{{ route('contato') }}">
                <div class="info_card">
                    <img src="{{ asset('assets/img/layout/ico-telefone.svg')}}" alt="">
                    <p>+55 {{$contato->celular}}</p>
                </div>
            </a>
            @endif
            @if($contato->endereco_pt)
            <a href="{{ route('contato') }}">
                <div class="info_card2">
                    <img src="{{ asset('assets/img/layout/ico-endereco.svg')}}" alt="">
                    <p>{{$contato->endereco_pt}} <br>{{$contato->bairro_pt}} <br>{{$contato->cep}} - {{$contato->cidade_pt}}</p>
                </div>
            </a>
            @endif
            @if($contato->instagram)
            <a href="{{$contato->instagram}}">
                <div class="info_card3">
                    <img src="{{ asset('assets/img/layout/ico-instagram.svg')}}" alt="">
                    <p>@cenografiasustentavel</p>
                </div>
            </a>
            @endif
        </article>

    </div>

    <div class="rights">
            <a href="{{ route('politica-de-privacidade') }}"><p>Política de Privacidade |</p></a>
            <p class="dados" style="text-transform: uppercase;"> © {{ date('Y') }} {{ config('app.name') }}</p>
            <p class="direitos"> • Todos os direitos reservados |</p>
            <a href="https://www.trupe.net" target="_blank" class="link-trupe"><p> Criação de sites: </p></a>
            <a href="https://www.trupe.net" target="_blank" class="link-trupe"><p>Trupe Agência Criativa</p></a>
    </div>
</footer>