<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateDestaquesTable extends Migration
{
    public function up()
    {
        Schema::create('destaques', function (Blueprint $table) {
            $table->id();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('alt');
            $table->string('titulo');
            $table->string('link');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('destaques');
    }
}
