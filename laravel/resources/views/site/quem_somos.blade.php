@extends('site.layout.template')

@section('content')

<main class="faixada">
    <img src="{{ asset('assets/img/layout/marca-cenografia-sustentavel.svg')}}" alt="logo" class="logo">
    <div class="texto">
        <h1>QUEM SOMOS</h1>
        <p>{!!$quem_somos->texto_1!!}</p>
    </div>

    @if($quem_somos->video)
    <div class="video">
        <div class="box">
        <iframe width="100%" height="100%" src="{{$quem_somos->video}}" title="YouTube video player"
            frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope;
            picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
    @endif

</main>

<section class="sustentabilidade">
    <div class="bloco">
        <div>
            <p class="p1">FOCO EM</p>
            <h1 class="p2">{{$quem_somos->titulo_2}}</h1>
        </div>
        <div class="frase">
            <p>{!!$quem_somos->texto_2!!}</p>
        </div>
    </div>
</section>

<section class="clientes">
    <h1>ALGUNS DE NOSSOS CLIENTES</h1>

        @if(count($clientes)<7)
        <div class="father2">
            @foreach($clientes as $cliente)
            <a href="{{$cliente->link}}">
                <img src="{{ asset('assets/img/clientes/'.$cliente->imagem) }}" alt="{{$cliente->alt}}">
            </a>
            @endforeach
        </div>
        @else
        <div class="father">
            @foreach($clientes as $cliente)
            <a href="{{$cliente->link}}">
                <img src="{{ asset('assets/img/clientes/'.$cliente->imagem) }}" alt="{{$cliente->alt}}">
            </a>
            @endforeach
        </div>
        @endif

</section>


@endsection