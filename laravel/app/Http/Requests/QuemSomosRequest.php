<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuemSomosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'texto_1' => 'required',
            'titulo_2' => 'required',
            'texto_2' => 'required',
     
        ];
    }
}
