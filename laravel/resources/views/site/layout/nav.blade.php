<a href="{{ route('home') }}" class="link-nav @if(Tools::routeIs('home*')) active @endif">home</a>
<a href="{{ route('somos') }}" class="link-nav @if(Tools::routeIs('somos*')) active @endif">quem somos</a>
<a href="" class="link-nav @if(Tools::routeIs('servicos*')) active @endif">nossos serviços</a>
<a href="" class="link-nav @if(Tools::routeIs('projetos*')) active @endif">projetos realizados</a>
<a href="" class="link-nav @if(Tools::routeIs('clipping*')) active @endif">clipping</a>

<a href="{{ route('contato') }}" class="link-nav @if(Tools::routeIs('contato*')) active @endif">contato</a>

<a href="" class="link-nav"><img src="{{ asset('assets/img/layout/ico-instagram-branco.svg') }}" alt=""></a>
