@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>BANNERS |</small> Segundo Painel</h2>
</legend>

{!! Form::model($parallax, [
'route' => ['parallax.update', $parallax->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.parallax.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection