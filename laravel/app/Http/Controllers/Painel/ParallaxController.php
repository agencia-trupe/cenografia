<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ParallaxRequest;
use App\Models\Parallax;
use Illuminate\Http\Request;

class ParallaxController extends Controller
{
    
    public function index()
    {
        $parallax = Parallax::first();

        return view('painel.parallax.edit', compact('parallax'));
    }

    public function update(ParallaxRequest $request, Parallax $parallax)
    {
        try {
            $input = $request->all();
            
            if (isset($input['imagem'])) $input['imagem'] = Parallax::upload_imagem();
            
            $parallax->update($input);
            
            return redirect()->route('parallax.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

}
