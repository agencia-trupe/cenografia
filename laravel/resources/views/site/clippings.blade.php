@extends('site.layout.template')

@section('content')

<section class="clippings">
    <div class="masonry grid" data-masonry='{ "itemSelector": ".grid-item", "columnWidth": 3}'>
        @foreach($clippingGaleria as $clipping)
        @foreach($imagens as $imagem)
        @if($clipping->id == $imagem->clipping_id)
        <a href="#" class="clipping2 clipping-galeria clipping-item grid-item" data-galeria-id="{{ $clipping->id }}">
            <div class="div-img">
                <img src="{{ asset('assets/img/clippings/imagens/'.$imagem->imagem) }}" class="img-capa" alt="">
                <div class="clip_info">
                    <img src="{{ asset('assets/img/layout/icone-clipping-arquivo.svg') }}" class="clip-icon">
                    <p class="titulo">{{ $clipping->titulo }}</p>
                </div>
            </div>
            @break
        </a>
        @endif
        @endforeach
        @endforeach

        @foreach($clippingLinks as $clipping)
        @foreach($links as $link)
        @if($clipping->id == $link->clipping_id)
        <a href="{{ $link->link_ext }}" target="_blank" class="clipping clipping-item grid-item">
            <div class="div-img ">
                <img src="{{ asset('assets/img/clippings/link/'.$link->capa) }}" class="img-capa" alt="">
                <div class="clip_info">
                    <img src="{{ asset('assets/img/layout/icone-clipping-link.svg') }}" class="clip-icon">
                    <p class="titulo">{{ $clipping->titulo }}</p>
                </div>
            </div>
        </a>
        @endif
        @endforeach
        @endforeach

        @foreach($clippingVideos as $clipping)
        @foreach($videos as $video)
        @if($clipping->id == $video->clipping_id)
        <a href="{{ $linkVideo.$video->link_video }}" target="_blank" class="clipping clipping-item grid-item">
            <div class="div-img">
                <iframe width="99%" height="225px" src="{{ $linkVideo.$video->link_video }}"></iframe><br>
                <div class="clip_info">
                    <img src="{{ asset('assets/img/layout/icone-clipping-video.svg') }}" class="clip-icon">
                    <p class="titulo">{{ $clipping->titulo }}</p>
                </div>
               
            </div>
        </a>
        @endif
        @endforeach
        @endforeach

        <div class="gutter"></div>
        <div class="sizer"></div>
    </div>

    <a class="btn-mais-clippings">ver mais</a>
</section>

<div class="imagens-show" style="display:none;">
    @foreach($clippingGaleria as $clipping)
    @foreach($imagens as $imagem)
    @if($clipping->id == $imagem->clipping_id)
    <a href="{{ asset('assets/img/clippings/imagens/'.$imagem->imagem) }}" class="fancybox" rel="galeria-{{ $clipping->id }}"></a>
    @endif
    @endforeach
    @endforeach
</div>


@endsection