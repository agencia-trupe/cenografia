<?php

namespace App\Http\Controllers;

use App\Models\ProjetoCategoria;
use App\Models\Projeto;
use App\Models\ProjetoImagem;
use App\Models\ProjetoLogo;
use Illuminate\Routing\Route;

class ProjetosController extends Controller
{
    public function __construct()
    {
        view()->share('categorias', ProjetoCategoria::ordenados()->get());
    }


    public function index($categoria_slug = null)
    {
        if ($categoria_slug == null) {

            $categoria = ProjetoCategoria::ordenados()->firstOrFail();
            $projetos2 = Projeto::ordenados()->get();

        } else {
            $categoria = ProjetoCategoria::where('slug', $categoria_slug)->first();

        }
        view()->share('categoria', $categoria);

        $projetos = $categoria->projetos;

        $projetos2 = Projeto::ordenados()->get();


        return view('site.projetos.index', compact('projetos','projetos2'));
    }

    public function index2($categoria_slug = null)
    {
        if ($categoria_slug == null) {

            $categoria = ProjetoCategoria::ordenados()->firstOrFail();

        } else {
            $categoria = ProjetoCategoria::where('slug', $categoria_slug)->first();

        }
        view()->share('categoria', $categoria);

        $projetos = $categoria->projetos;

        return view('site.projeto.index', compact('projetos'));
    }


    public function show($categoria_slug, $projeto_slug)
    {
        $projeto = Projeto::where('slug', $projeto_slug)->first();
        $imagens = ProjetoImagem::projeto($projeto->id)->ordenados()->get();
        $logos = ProjetoLogo::where('projeto_id', $projeto->id)->ordenados()->get();

        return view('site.projetos.show', compact('projeto', 'imagens', 'logos'));
    }

}
