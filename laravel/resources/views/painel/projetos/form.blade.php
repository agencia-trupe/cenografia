@include('painel.layout.flash')

<div class="form-group">
    {!! Form::label('projetos_categoria_id', 'Categoria') !!}
    {!! Form::select('projetos_categoria_id', $categorias, old('projetos_categoria_id'), ['class' => 'form-control']) !!}
</div>


<div class="form-group mt-3">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>


<div class="mb-3 col-12 col-md-12 mt-3">
    {!! Form::label('capa', 'Capa') !!}
    @if($submitText == 'Alterar')
    @if($projeto->capa)
    <img src="{{ url('assets/img/projetos/'.$projeto->capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    @endif
    {!! Form::file('capa', ['class' => 'form-control']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('alt', 'Descrição da Imagem (Alt)', ['class' => 'form-label']) !!}
    @if($submitText == 'Alterar')
    {!! Form::text('alt', null, ['class' => 'form-control input-text']) !!}
    @else
    {!! Form::text('alt', null, ['class' => 'form-control input-text']) !!}
    @endif
</div>


<div class="mb-3 col-12">
    {!! Form::label('descricao', 'Descritivo do Projeto') !!}
    {!! Form::textarea('descricao', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="form-group mt-3">
    {!! Form::label('local', 'Local') !!}
    {!! Form::text('local', null, ['class' => 'form-control']) !!}
</div>

<h4 class="mt-5">Campos de SEO para otimização de buscadores</h4>

<div class="row mb-2">
    <div class="mb-3 col-6 col-md-6">
        {!! Form::label('title', 'Título SEO') !!}
        {!! Form::text('title', null, ['class' => 'form-control input-text']) !!}
    </div>

    <div class="mb-3 col-6 col-md-6">
        {!! Form::label('keywords', 'Keywords SEO') !!}
        {!! Form::text('keywords', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('description', 'Descrição do produto SEO') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control editor-padrao']) !!}
</div>


{!! Form::submit($submitText, ['class' => 'btn btn-success mt-3']) !!}

<a href="{{ route('projetos.index') }}" class="btn btn-default btn-light mt-3">Voltar</a>
