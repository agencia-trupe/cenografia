@extends('site.layout.template')

@section('content')

<main class="home">

    <div>
        <div class="banners">

            <!-- Start WOWSlider.com BODY section -->
            <div id="wowslider-container1">
                <div class="ws_images"><ul>
                    @foreach($banners as $banner)
                        <li>
                            <a href="{{$banner->link}}">
                                <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" title="{{$banner->frase}}" alt="{{$banner->alt}}" id="wows1_0"/>
                            </a>
                        </li>
                    @endforeach
                    </ul></div>
                    {{-- <img src="{{ asset('assets/img/layout/marca-cenografia-sustentavel.svg')}}" class="logo" alt=""> --}}
                <div class="ws_shadow"></div>
            </div>
            <script type="text/javascript" src="{{ asset('assets/js/wowslider/engine1/wowslider.js') }}"></script>
            <script type="text/javascript" src="{{ asset('assets/js/wowslider/engine1/script.js') }}"></script>

            <!-- End WOWSlider.com BODY section -->

        </div>

        <div>
            <img src="{{ asset('assets/img/layout/marca-cenografia-sustentavel.svg')}}" class="logo" alt="">
        </div>
    </div>
</main>

<section class="cenografia">

    <div class="home_fichas">
        <a href="{{$home->link_1}}">
            <div class="ficha_1">
                <p class="hover_circle"></p>
                <img src="{{ asset('assets/img/layout/bola-destaques-home.svg')}}" alt="">
                <p>{{$home->ficha_1}}</p>
            </div>
        </a>
        <a href="{{$home->link_2}}">
            <div class="ficha_1">
                <p class="hover_circle"></p>
                <img src="{{ asset('assets/img/layout/bola-destaques-home.svg')}}" alt="">
                <p>{{$home->ficha_2}}</p>
            </div>
        </a>
        <a href="{{$home->link_3}}">
            <div class="ficha_1">
                <p class="hover_circle"></p>
                <img src="{{ asset('assets/img/layout/bola-destaques-home.svg')}}" alt="">
                <p>{{$home->ficha_3}}</p>
            </div>
        </a>
        <a href="{{$home->link_4}}">
            <div class="ficha_1">
                <p class="hover_circle"></p>
                <img src="{{ asset('assets/img/layout/bola-destaques-home.svg')}}" alt="">
                <p>{{$home->ficha_4}}</p>
            </div>
        </a>
    </div>


    <!-- MOBILE -->
    <div class="mobile_fichas">
        <a href="{{$home->link_1}}" class="ficha">
            <p>{{$home->ficha_1}}</p>
        </a>
        <a href="{{$home->link_2}}" class="ficha">
            <p>{{$home->ficha_2}}</p>
        </a>
        <a href="{{$home->link_3}}" class="ficha">
            <p>{{$home->ficha_3}}</p>
        </a>
        <a href="{{$home->link_4}}" class="ficha">
            <p>{{$home->ficha_4}}</p>
        </a>
    </div>


    <div class="sustentavel">
        <p>{{ str_replace(array("<p>", "</p>","<strong>","</strong>","<b>", "</b>"),'', $home->frase_1) }}</p>
        @if(@isset($home->link_video))
            <div class="video_home">
                <iframe width="100%" height="100%" src="{{ $home->link_video }}"
                    title="YouTube video player" frameborder="0" allow="accelerometer; autoplay;
                    clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                </iframe>
            </div>
        @endisset
    </div>

</section>


<section class="parallax">

    <div class="bg" style="background-image: url({{ asset('assets/img/banners/'.$parallax->imagem) }})" alt="{{$parallax->alt}}"></div>
    <div class="conteudo">
        <p>{{ str_replace(array("<p>", "</p>","<strong>","</strong>","<b>", "</b>"),'', $home->frase_2) }}</p>
        <a href="{{$home->botao}}">SAIBA MAIS »</a>
    </div>

</section>


<section class="home_balls">

    <h1>CONFIRA ALGUNS PROJETOS EM DESTAQUE</h1>

    <ul id="da-thumbs" class="da-thumbs father">
        @foreach($destaques as $destaque)
        <li class="ext_ball">
            <a href="{{$destaque->link}}" class="ball">
                <img src="{{ asset('assets/img/destaques/'.$destaque->imagem) }}" alt="{{$destaque->alt}}">
                <div class="overlay"></div>
            </a>
            <span class="tag">
                <div class="one">
                    <p>{{$destaque->titulo}}</p>
                </div>
                <div class="two"></div>
            </span>
        </li>
        @endforeach
    </ul>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        $(function() {

            $(' #da-thumbs > li ').each( function() { $(this).hoverdir(); } );

        });
    </script>

</section>

<section class="book">

    <div class="centre">
        <div class="mainFrases">
            <h3>{{$livro->titulo}}</h1>
            <h4>{!! $livro->subtitulo !!}</h4>
        </div>
        <div class="mainBox">
            <div class="imagemLivro">
                <img src="{{ asset('assets/img/livros/'.$livro->imagem) }}" alt="{{$livro->alt}}">
            </div>
            <div class="textoLivro">
                <p>{!! $livro->texto !!}</p>
            </div>
            <a href="{{$livro->link_botao}}">
                <div class="btnLivro">
                    <p>{{$livro->botao}}</p>
                </div>
            </a>
        </div>
    </div>

</section>


@endsection