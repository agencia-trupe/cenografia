@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::text('email', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('telefone', 'Telefone') !!}
        {!! Form::text('telefone', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-6">
        {!! Form::label('celular', 'Celular/Whatsapp') !!}
        {!! Form::text('celular', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="mb-3 col-12">
    {!! Form::label('instagram', 'Instagram (link)') !!}
    {!! Form::text('instagram', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('endereco_pt', 'Endereço (PT)') !!}
        {!! Form::text('endereco_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('endereco_en', 'Endereço (EN)') !!}
        {!! Form::text('endereco_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('endereco_es', 'Endereço (ES)') !!}
        {!! Form::text('endereco_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('bairro_pt', 'Bairro (PT)') !!}
        {!! Form::text('bairro_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('bairro_en', 'Bairro (EN)') !!}
        {!! Form::text('bairro_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('bairro_es', 'Bairro (ES)') !!}
        {!! Form::text('bairro_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('cidade_pt', 'Cidade/UF (PT)') !!}
        {!! Form::text('cidade_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('cidade_en', 'Cidade/UF (EN)') !!}
        {!! Form::text('cidade_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('cidade_es', 'Cidade/UF (ES)') !!}
        {!! Form::text('cidade_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="row mb-2">
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('pais_pt', 'País (PT)') !!}
        {!! Form::text('pais_pt', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('pais_en', 'País (EN)') !!}
        {!! Form::text('pais_en', null, ['class' => 'form-control input-text']) !!}
    </div>
    <div class="mb-3 col-12 col-md-4">
        {!! Form::label('pais_es', 'País (ES)') !!}
        {!! Form::text('pais_es', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="mb-3 col-12">
    {!! Form::label('cep', 'CEP') !!}
    {!! Form::text('cep', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('google_maps', 'Código GoogleMaps') !!}
    {!! Form::text('google_maps', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('contatos.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>