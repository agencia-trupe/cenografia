@include('painel.layout.flash')

<div class="mb-3 col-12">
    {!! Form::label('texto_1', 'Texto Quem Somos') !!}
    {!! Form::textarea('texto_1', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<hr class="mt-5 mb-4">

<div class="mb-3 col-12">
    {!! Form::label('video', 'URL de incorporação do Vídeo (exemplo: https://www.youtube.com/embed/dvAbtpCxbCE)') !!}
    {!! Form::text('video', null, ['class' => 'form-control']) !!}
</div>

<hr class="mt-5 mb-4">

<div class="mb-3 col-12">
    {!! Form::label('titulo_2', 'Titulo do Foco') !!}
    {!! Form::text('titulo_2', null, ['class' => 'form-control']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('texto_2', 'Texto do Foco') !!}
    {!! Form::textarea('texto_2', null, ['class' => 'form-control editor-padrao']) !!}
</div>


<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('quem-somos.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>