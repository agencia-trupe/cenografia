@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>DESTAQUES |</small> Editar Destaque</h2>
</legend>

{!! Form::model($destaque, [
'route' => ['destaques.update', $destaque->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.destaques.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection