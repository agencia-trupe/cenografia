<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\LivrosRequest;
use App\Models\Livro;

class LivrosController extends Controller
{
    public function index()
    {
        $livro = Livro::first();

        return view('painel.livros.edit', compact('livro'));
    }

    public function store(LivrosRequest $request)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Livro::upload_imagem();

            Livro::create($input);

            return redirect()->route('livros.index')->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function update(LivrosRequest $request, Livro $livro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Livro::upload_imagem();

            $livro->update($input);

            return redirect()->route('livros.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Livro $livro)
    {
        try {
            $livro->delete();

            return redirect()->route('livro.index')->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }

}
