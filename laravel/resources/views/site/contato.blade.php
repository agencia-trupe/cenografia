@extends('site.layout.template')

@section('content')

<main class="faixada">
        <img src="{{ asset('assets/img/layout/marca-cenografia-sustentavel.svg')}}" alt="logo" class="logo">
        <div class="texto contatotext">
            <h1>CONTATO</h1>
        </div>

        <div class="dados-form">

            <article class="informacoes_a">
            @php
            $telefone = str_replace(array(" ", "-"), '', $contato->telefone);
            $celular = str_replace(array(" ", "-"), '', $contato->celular);
            @endphp

            @if($contato->celular)
                <a href="https://api.whatsapp.com/send?phone=55{!!$celular!!}&text=ol%C3%A1%20tudo%20bem">
                <div class="info_card_a">
                    <img src="{{ asset('assets/img/layout/ico-whatsapp.svg')}}" alt="">
                    <p>+55 {{ $contato->celular }}</p>
                </div>
                </a>
            @endif
            @if($contato->telefone)
                <a href="tel:+55{!!$telefone!!}">
                    <div class="info_card_a">
                        <img src="{{ asset('assets/img/layout/ico-telefone.svg')}}" alt="">
                        <p>+55 {{ $contato->telefone }}</p>
                    </div>
                </a>
            @endif
            @if($contato->endereco_pt)
                <a href="{{ route('contato') }}">
                    <div class="info_card2_a">
                        <img src="{{ asset('assets/img/layout/ico-endereco.svg')}}" alt="">
                        <p>{{$contato->endereco_pt}} <br>{{$contato->bairro_pt}} <br> {{$contato->cidade_pt}}</p>                </p>
                    </div>
                </a>
            @endif
            @if($contato->instagram)
                <a href="{{$contato->instagram}}">
                    <div class="info_card3_a">
                        <img src="{{ asset('assets/img/layout/ico-instagram.svg')}}" alt="">
                        <p>@cenografiasustentavel</p>
                    </div>
                </a>
            @endif
            </article>

            <form action="{{ route('contato.post') }}" method="POST" class="form-contato" enctype="multipart/form-data">
                {!! csrf_field() !!}
                
                <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" class="inputs" required>
                <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" class="inputs" required>
                <input type="text" name="telefone" placeholder="telefone" class="inputs" value="{{ old('telefone') }}">
                <textarea name="mensagem" placeholder="mensagem" class="inputs_text" required>{{ old('mensagem') }}</textarea>
                <button type="submit" class="btn-enviar">ENVIAR <img src="{{ asset('assets/img/layout/icone-enviiar.svg')}}" alt=""></button>
                    
                
            </form>

            @if($errors->any())
            <div class="flash flash-erro">
                @foreach($errors->all() as $error)
                {!! $error !!}<br>
                @endforeach
            </div>
            @endif

            @if(session('enviado'))
            <div class="flash flash-sucesso">
                <p>Mensagem enviada com Sucesso</p>
            </div>
            @endif

        </div>

        <section class="endereco-mapa">

            <article class="mapa">
                {{-- {!! $contato->google_maps !!} --}}
                <iframe src="{!! $contato->google_maps !!}" 
                width="100%" height="400" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </article>

        </section>

    </div>

</main>

@endsection