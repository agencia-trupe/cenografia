@include('painel.layout.flash')

<h2 class="mt-5 mb-4">Chamada Principal</h2>

<div class="mb-3 col-12">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<hr class="mt-5 mb-4">

<div class="mb-3 col-12">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control editor-padrao']) !!}
</div>


<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('quem-somos.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>