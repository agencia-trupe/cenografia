<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProjetosLogosRequest;
use App\Models\ProjetoLogo;
use App\Models\Projeto;

class ProjetosLogosController extends Controller
{
    public function index(Projeto $projeto)
    {
        $registros = $projeto->logos;

        return view('painel.projetos.logos.index', compact('projeto', 'registros'));
    }

    public function create(Projeto $projeto)
    {
        $projeto_id = $projeto->id;

        return view('painel.projetos.logos.create', compact('projeto'));
    }

    public function store(ProjetosLogosRequest $request, Projeto $projeto)
    {
        try {
            $input = $request->all();

            if (isset($input['logo'])) $input['logo'] = ProjetoLogo::upload_imagem();

            ProjetoLogo::create($input);

            return redirect()->route('projetos.logos.index', ['projeto' => $projeto->id])->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Projeto $projeto, ProjetoLogo $logo)
    {
        return view('painel.projetos.logos.edit', compact('projeto', 'logo'));
    }

    public function update(ProjetosLogosRequest $request, Projeto $projeto, ProjetoLogo $logo)
    {
        try {

            $input = $request->all();

            $logo->update($input);

            return redirect()->route('projetos.logos.index', $projeto->id)
                ->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Projeto $projeto, ProjetoLogo $logo)
    {
        try {

            $logo->delete();

            return redirect()->route('projetos.logos.index', $projeto->id)
                ->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
