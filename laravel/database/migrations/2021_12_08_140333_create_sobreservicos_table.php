<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ServicesTable extends Migration
{
    public function up()
    {
        Schema::create('sobre_servicos', function (Blueprint $table) {
            $table->id();
            $table->text('titulo');
            $table->string('texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sobre_servicos');
    }
}
