@include('painel.layout.flash')

<div class="form-group">
    {!! Form::label('tipo_id', 'Tipos') !!}
    {!! Form::select('tipo_id', $tipos , old('tipo_id'), ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('clippings.index') }}" class="btn btn-default btn-voltar">Voltar</a>