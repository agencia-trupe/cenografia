<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SobreServicos extends Model
{
    use HasFactory;

    protected $table = 'sobre_servicos';

    protected $guarded = ['id'];

}
