<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateHomeTable extends Migration
{
    public function up()
    {
        Schema::create('home', function (Blueprint $table) {
            $table->id();
            $table->string('call');
            $table->text('frase_1');
            $table->text('frase_2');
            $table->text('link_video')->nullable();
            $table->string('ficha_1')->nullable();
            $table->string('link_1')->nullable();
            $table->string('ficha_2')->nullable();
            $table->string('link_2')->nullable();
            $table->string('ficha_3')->nullable();
            $table->string('link_3')->nullable();
            $table->string('ficha_4')->nullable();
            $table->string('link_4')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('home');
    }
}
