@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">Projetos</h2>
    <div class="d-flex flex-row align-items-center">
        <a href="{{ route('categorias.index') }}" class="btn btn-primary btn-sm pull-right"><span class="glyphicon glyphicon-edit" style="margin-right:10px;"></span>Editar Categorias</a>
        <a href="{{ route('projetos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Projeto</a>
    </div>
</legend>

<div class="mt-3 mb-5">
    <h4>Categorias:</h4>
    <div class="row ps-2">
        <a href="{{ route('projetos.index') }}" class="btn btn-secondary col-12 col-md-2 m-1">TODOS</a>
        @foreach($categorias as $categoria)
            <a href="{{ route('projetos.index', ['categoria' => $categoria->id]) }}" class="btn btn-secondary col-12 col-md-2 m-1 {{ (isset($_GET['categoria']) && $_GET['categoria'] == $categoria->id) ? 'active' : '' }}">{{ $categoria->titulo }}</a>
        @endforeach
    </div>
</div>

@if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-sortable" data-table="projetos">
    <thead>
        <tr>
            <th>Ordenar</th>
            <th>Categoria</th>
            <th>Título</th>
            <th>Capa</th>
            <th>Imagens</th>
            <th>Vídeos</th>
            <th>Logos</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($registros as $registro)
        <tr class="tr-row" id="{{ $registro->id }}">
            <td>
                <a href="#" class="btn btn-dark btn-sm btn-move">
                    <i class="bi bi-arrows-move"></i>
                </a>
            </td>
            <td>
                @if($registro->categoria)
                    {{ $registro->categoria }}
                @else
                    <span class="label label-warning">sem categoria</span>
                @endif
            </td>
            <td>{{ $registro->titulo }}</td>
            <td><img src="{{ asset('assets/img/projetos/'.$registro->capa) }}" style="width: 100%; max-width:80px;" alt=""></td>
            <td>
                <a href="{{ route('projetos.imagens.index', $registro->id) }}" class="btn btn-secondary btn-gerenciar btn-sm">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>Gerenciar
                </a>
            </td>
            <td>
                <a href="{{ route('projetos.videos.index', $registro->id) }}" class="btn btn-secondary btn-gerenciar btn-sm">
                    <span class="glyphicon glyphicon-facetime-video" style="margin-right:10px;"></span>Gerenciar
                </a>
            </td>
            <td>
                <a href="{{ route('projetos.logos.index', $registro->id) }}" class="btn btn-secondary btn-gerenciar btn-sm">
                    <span class="glyphicon glyphicon-facetime-video" style="margin-right:10px;"></span>Gerenciar
                </a>
            </td>
            <td class="crud-actions">
                {!! Form::open([
                'route' => ['projetos.destroy', $registro->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('projetos.edit', $registro->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection