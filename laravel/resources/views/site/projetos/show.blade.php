@extends('site.layout.template')

@section('content')

<main class="faixada">
    <img src="{{ asset('assets/img/layout/marca-cenografia-sustentavel.svg')}}" alt="logo" class="logo">
    <div class="texto contatotext">
        <h1>PROJETOS REALIZADOS</h1>
    </div>

    <div class="sub_categoria">
            <a href="{{ route('projetos') }}">
                <div @if(Tools::routeIs('projetos')) class="active" @else class="categoria" @endif>
                    <p>todos</p>
                </div>
            </a>
        @foreach($categorias as $c)
            <a href="{{ route('projeto', ['categoria_slug' => $c->slug]) }}">
                <div @if(isset($categoria) && $categoria->slug == $c->slug) class="active" @else class="categoria" @endif>
                    <p>{{ $c->titulo }}</p>
                </div>
            </a>
        @endforeach
    </div>

    <div class="center-full" id="center-full">
        <div class="pshow-center">
            <div class="first">
                <div class="capa">
                    <img src="{{ asset('assets/img/projetos/'.$projeto->capa) }}" alt="{{$projeto->alt}}" class="img-projeto">
                </div>

                <div class="rightSide">
                    <div class="descricao">
                        <img src="{{ asset('assets/img/layout/setinha-banner-home.svg') }}" alt="">
                        <h2>{{ $projeto->titulo }}</h2>
                        <p>{{ $projeto->local }}</p>
                        <p>{!!$projeto->descricao!!}</p>
                    </div>

                    <div class="flexLogos">
                        @if(count($logos))
                        @foreach($logos as $logo)
                            <a href="{{$logo->link}}">
                                <img src="{{ asset('assets/img/logos/'.$logo->logo) }}" alt="{{$logo->alt}}" class="logosImg">
                            </a>
                        @endforeach
                        @endif
                    </div>
                </div>

            </div>
            <div class="pshow-videobox">
                @if(count($projeto->videos))
                @foreach($projeto->videos as $v)

                    <div class="videos">
                        <iframe src="{{ $v->video }}" width="100%" height="100%" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>

                @endforeach
                @endif
            </div>

            <div class="pshow-imagembox grid" data-masonry='{ "itemSelector": ".grid-item", "columnWidth": 200}'>
                @if(count($projeto->imagens))
                @foreach($projeto->imagens as $i)
                <a href="{{ asset('assets/img/projetos/imagens/highres/'.$i->imagem) }}" class="projeto-imagem" rel="projeto" >
                    <img src="{{ asset('assets/img/projetos/imagens/'.$i->imagem) }}" alt="" class="img-projeto grid-item">
                </a>
                @endforeach
                @endif
            </div>
        </div>

        {{-- <div class="controls">
            <a href="{{ route('projetos', $projeto->categoria->slug) }}">
                <div class="link-topo-box">
                    <img src="{{ asset('assets/img/layout/ico-seta-voltar.png') }}" class="ico-seta-topo" alt="">
                    <p>voltar</p>
                </div>
            </a>

            <a href="" class="link-topo" title="Voltar ao Topo">
                <div class="link-topo-box">
                    <p class="control-voltar">topo</p>
                    <img src="{{ asset('assets/img/layout/ico-seta-topo.png') }}" class="ico-seta-topo" alt="">
                </div>
            </a>

        </div> --}}
    </div>

</main>
@endsection
