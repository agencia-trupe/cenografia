<?php

namespace App\Http\Controllers;

use App\Models\AceiteDeCookies;
use App\Models\Banner;
use App\Models\Destaque;
use App\Models\Parallax;
use App\Models\Home;
use App\Models\Livro;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $banners = Banner::ordenados()->get();
        $parallax = Parallax::first();
        $home = Home::first();
        $destaques = Destaque::ordenados()->get();
        $livro = Livro::first();

        return view('site.home', compact('banners','parallax','home','destaques', 'livro'));
    }

    public function postCookies(Request $request)
    {
        $input = $request->all();
        $input['ip'] = $request->ip();

        AceiteDeCookies::create($input);

        return redirect('/');
    }
}
