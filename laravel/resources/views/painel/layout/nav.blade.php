<ul class="nav navbar-nav">


    <li class="nav-item dropdown">
        <a href="#" class="nav-link dropdown-toggle px-3 @if(Tools::routeIs(['banners*'])) active @elseif(Tools::routeIs('parallax*')) active @endif d-flex align-items-center" role="button" id="navbarDarkDropdownMenuContatos" data-bs-toggle="dropdown" aria-expanded="false">
            Banners
            <i class="bi bi-caret-down-fill ms-1"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark" style="min-width: 160px" aria-labelledby="navbarDarkDropdownMenuBanners">
            <li>
                <a href="{{ route('banners.index') }}" class="nav-link px-3 @if(Tools::routeIs('banners*')) active @endif">Painel Principal</a>
            </li>
            <li>
                <a href="{{ route('parallax.index') }}" class="nav-link px-3 @if(Tools::routeIs('parallax*')) active  @endif">Segundo Painel</a>
            </li>
        </ul>
    </li>

    <li class="nav-item dropdown">
        <a href="#" class="nav-link dropdown-toggle px-3 @if(Tools::routeIs(['home*'])) active @elseif(Tools::routeIs('destaques*')) active @endif d-flex align-items-center" role="button" id="navbarDarkDropdownMenuContatos" data-bs-toggle="dropdown" aria-expanded="false">
            Home
            <i class="bi bi-caret-down-fill ms-1"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark" style="min-width: 180px" aria-labelledby="navbarDarkDropdownMenuContatos">
            <li>
                <a href="{{ route('home.index') }}" class="nav-link px-3 @if(Tools::routeIs('home*')) active @endif">Home</a>
            </li>
            <li>
                <a href="{{ route('destaques.index') }}" class="nav-link px-3 @if(Tools::routeIs('destaques*')) active @endif">Projetos em Destaque</a>
            </li>
            <li>
                <a href="{{ route('livros.index') }}" class="nav-link px-3 @if(Tools::routeIs('livros*')) active @endif">Chamada do Livro</a>
            </li>
        </ul>
    </li>

    <li class="nav-item dropdown">
        <a href="#" class="nav-link dropdown-toggle px-3 @if(Tools::routeIs(['quem-somos*'])) active @elseif(Tools::routeIs('clientes*')) active @endif d-flex align-items-center" role="button" id="navbarDarkDropdownMenuQuemSomos" data-bs-toggle="dropdown" aria-expanded="false">
            Quem Somos
            <i class="bi bi-caret-down-fill ms-1"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark" style="min-width: 160px" aria-labelledby="navbarDarkDropdownMenuQuemSomos">
            <li>
                <a href="{{ route('quem-somos.index') }}" class="nav-link px-3 @if(Tools::routeIs('quem-somos*')) active @endif">Quem Somos</a>
            </li>
            <li>
                <a href="{{ route('clientes.index') }}" class="nav-link px-3 @if(Tools::routeIs('clientes*')) active @endif">Clientes</a>
            </li>
        </ul>
    </li>

    <li class="nav-item dropdown">
        <a href="#" class="nav-link dropdown-toggle px-3 @if(Tools::routeIs(['servicos*'])) active @elseif(Tools::routeIs('sobre-servicos*')) active @endif d-flex align-items-center" role="button" id="navbarDarkDropdownMenuContatos" data-bs-toggle="dropdown" aria-expanded="false">
            Serviços
            <i class="bi bi-caret-down-fill ms-1"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark" style="min-width: 160px" aria-labelledby="navbarDarkDropdownMenuBanners">
            <li>
                <a href="{{ route('servicos.index') }}" class="nav-link px-3 @if(Tools::routeIs('servicos*')) active @endif">Serviços</a>
            </li>
            <li>
                <a href="{{ route('sobre-servicos.index') }}" class="nav-link px-3 @if(Tools::routeIs('sobre-servicos*')) active  @endif">Chamada Principal</a>
            </li>
        </ul>
    </li>

    <li>
        <a href="{{ route('projetos.index') }}" class="nav-link px-3 @if(Tools::routeIs('projetos*')) active @endif">Projetos</a>
    </li>

    <li>
        <a href="{{ route('clippings.index') }}" class="nav-link px-3 @if(Tools::routeIs('clippings*')) active @endif">Clipping</a>
    </li>

    <li class="nav-item dropdown">
        <a href="#" class="nav-link dropdown-toggle px-3 @if(Tools::routeIs(['contatos*'])) active @endif d-flex align-items-center" role="button" id="navbarDarkDropdownMenuContatos" data-bs-toggle="dropdown" aria-expanded="false">
            Contatos
            @if($contatosNaoLidos >= 1)
            <span class="label label-success ms-1">{{ $contatosNaoLidos }}</span>
            @endif
            <i class="bi bi-caret-down-fill ms-1"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuContatos">
            <li>
                <a href="{{ route('contatos.index') }}" class="dropdown-item @if(Tools::routeIs('contatos.index')) active @endif">Informações de contato</a>
            </li>
            <li>
                <a href="{{ route('contatos-recebidos.index') }}" class="dropdown-item @if(Tools::routeIs('contatos-recebidos*')) active @endif d-flex align-items-center">
                    Contatos recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success ms-1">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
        </ul>
    </li>

</ul>