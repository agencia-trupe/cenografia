@extends('site.layout.template')

@section('content')

<main class="faixada">
    <img src="{{ asset('assets/img/layout/marca-cenografia-sustentavel.svg')}}" alt="logo" class="logo">
    <div class="texto contatotext">
        <h1>PROJETOS REALIZADOS</h1>
    </div>

    <div class="sub_categoria">
            <a href="{{ route('projetos') }}">
                <div class="categoria">
                    <p>todos</p>
                </div>
            </a>
        @foreach($categorias as $c)
            <a href="{{ route('projeto', ['categoria_slug' => $c->slug]) }}">
                <div @if(isset($categoria) && $categoria->slug == $c->slug) class="active" @else class="categoria" @endif>
                    <p>{{ $c->titulo }}</p>
                </div>
            </a>
        @endforeach
    </div>

    <div class="center">

        <ul id="da-thumbs" class="da-thumbs index-grid">
            @foreach($projetos as $p)
                <li class="ext_card">
                    <a href="{{ route('projetos-show', ['categoria_slug' => $p->categoria->slug, 'projeto_slug' => $p->slug]) }}" class="card">
                        <img src="{{ asset('assets/img/projetos/thumbs/'.$p->capa) }}" alt="{{$p->alt}}">
                        <div class="overlay">
                            <h1>{{$p->titulo}}</h1>
                            <h2>{{$p->local}}</h2>
                        </div>
                    </a>
                </li>
            @endforeach
        </ul>

    </div>


</main>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        $(function() {

            $(' #da-thumbs > li ').each( function() { $(this).hoverdir(); } );

        });
    </script>


@endsection
