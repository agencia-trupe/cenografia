@include('painel.layout.flash')

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($submitText == 'Alterar')
    @if($servico->imagem)
    <img src="{{ url('assets/img/servicos/'.$servico->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('alt', 'Descrição da Imagem (Alt)', ['class' => 'form-label']) !!}
    @if($submitText == 'Alterar')
    {!! Form::text('alt', null, ['class' => 'form-control input-text']) !!}
    @else
    {!! Form::text('alt', null, ['class' => 'form-control input-text']) !!}
    @endif
</div>

<div class="mb-3">
    {!! Form::label('titulo', 'Titulo', ['class' => 'form-label']) !!}
    {!! Form::text('titulo', null, ['class' => 'form-control input-text']) !!}
</div>

<div class="mb-3">
    {!! Form::label('texto', 'Texto', ['class' => 'form-label']) !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control editor-padrao']) !!}
</div>


<div class="mb-3">
    {!! Form::label('slug', 'Slug (Não permite espaços e caracteres especiais exceto traço e underline)', ['class' => 'form-label']) !!}
    {!! Form::text('slug', null, ['class' => 'form-control input-text']) !!}
</div>

<h4 class="mt-5">Campos de SEO para otimização de buscadores</h4>

<div class="row mb-2">
    <div class="mb-3 col-6 col-md-6">
        {!! Form::label('title', 'Título SEO') !!}
        {!! Form::text('title', null, ['class' => 'form-control input-text']) !!}
    </div>

    <div class="mb-3 col-6 col-md-6">
        {!! Form::label('keywords', 'Keywords SEO') !!}
        {!! Form::text('keywords', null, ['class' => 'form-control input-text']) !!}
    </div>
</div>

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('description', 'Descrição do produto SEO') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control editor-padrao']) !!}
</div>


<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('servicos.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>
