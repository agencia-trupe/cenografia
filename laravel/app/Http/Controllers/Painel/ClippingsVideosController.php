<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClippingVideosRequest;
use App\Models\Clipping;
use App\Models\ClippingVideo;

class ClippingsVideosController extends Controller
{
    public function index(Clipping $clipping)
    {
        $video = ClippingVideo::where('clipping_id', $clipping->id)->first();

        return view('painel.clippings.video.index', compact('clipping', 'video'));
    }

    public function create(Clipping $clipping)
    {
        return view('painel.clippings.video.create', compact('clipping'));
        
    }

    public function store(ClippingVideosRequest $request, Clipping $clipping)
    {
        try {
            $input = $request->all();
            $input['clipping_id'] = $clipping->id;

            ClippingVideo::create($input);

            return redirect()->route('clippings.videos.index', $clipping->id)->with('success', 'Registro adicionado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: ' . $e->getMessage()]);
        }
    }

    public function edit(Clipping $clipping, ClippingVideo $video)
    {
        return view('painel.clippings.video.edit', compact('clipping', 'video'));
    }

    public function update(ClippingVideosRequest $request, Clipping $clipping, ClippingVideo $video)
    {
        try {
            $input = $request->all();
            $input['clipping_id'] = $clipping->id;

            $video->update($input);

            return redirect()->route('clippings.videos.index', $clipping->id)->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: ' . $e->getMessage()]);
        }
    }

    public function destroy(Clipping $clipping, ClippingVideo $video)
    {
        try {
            $video->delete();

            return redirect()->route('clippings.videos.index', $clipping->id)->with('success', 'Registro excluído com sucesso.');
        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: ' . $e->getMessage()]);
        }
    }
}
