@extends('painel.layout.template')

@section('content')

    <legend class="mb-4">
        <h2 class="m-0"><small>LOGOS |</small> Adicionar Logo</h2>
    </legend>

    {!! Form::open(['route' => ['projetos.logos.store', 'projeto' => $projeto->id], 'files' => true]) !!}

        @include('painel.projetos.logos.form', ['submitText' => 'Adicionar'])

    {!! Form::close() !!}

@endsection