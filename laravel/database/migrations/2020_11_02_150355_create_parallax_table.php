<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateParallaxTable extends Migration
{
    public function up()
    {
        Schema::create('parallax', function (Blueprint $table) {
            $table->id();
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('alt');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('parallax');
    }
}
