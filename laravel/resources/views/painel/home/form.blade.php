@include('painel.layout.flash')

<h3 style="padding-top: 40px;">Frases e Links das Etiquetas</h3>

<h4 class="mt-5">Etiqueta 1</h4>
<div class="mb-6 col-12 d-flex mt-3" style="column-gap: 25px; border: 1px solid #d1d3d6; padding-right: 30px;padding-left: 30px; border-radius: 6px;">
    <div class="" style="padding-top: 25px;padding-bottom: 25px;width: 47%">
        {!! Form::label('ficha_1', ' Titulo') !!}
        {!! Form::text('ficha_1', null, ['class' => 'form-control']) !!}
    </div>
    <div class="" style="padding-top: 25px;padding-bottom: 25px;width: 47%">
        {!! Form::label('link_1', 'Link') !!}
        {!! Form::text('link_1', null, ['class' => 'form-control']) !!}
    </div>
</div>

<h4 class="mt-5">Etiqueta 2</h4>
<div class="mb-6 col-12 d-flex mt-3" style="column-gap: 25px; border: 1px solid #d1d3d6; padding-right: 30px;padding-left: 30px; border-radius: 6px;">
    <div class="" style="padding-top: 25px;padding-bottom: 25px;width: 47%">
        {!! Form::label('ficha_2', ' Titulo') !!}
        {!! Form::text('ficha_2', null, ['class' => 'form-control']) !!}
    </div>
    <div class="" style="padding-top: 25px;padding-bottom: 25px;width: 47%">
        {!! Form::label('link_2', 'Link') !!}
        {!! Form::text('link_2', null, ['class' => 'form-control']) !!}
    </div>
</div>

<h4 class="mt-5">Etiqueta 3</h4>
<div class="mb-6 col-12 d-flex mt-3" style="column-gap: 25px; border: 1px solid #d1d3d6; padding-right: 30px;padding-left: 30px; border-radius: 6px;">
    <div class="" style="padding-top: 25px;padding-bottom: 25px;width: 47%">
        {!! Form::label('ficha_3', ' Titulo') !!}
        {!! Form::text('ficha_3', null, ['class' => 'form-control']) !!}
    </div>
    <div class="" style="padding-top: 25px;padding-bottom: 25px;width: 47%">
        {!! Form::label('link_3', 'Link') !!}
        {!! Form::text('link_3', null, ['class' => 'form-control']) !!}
    </div>
</div>

<h4 class="mt-5">Etiqueta 4</h4>
<div class="mb-6 col-12 d-flex mt-3" style="column-gap: 25px; border: 1px solid #d1d3d6; padding-right: 30px;padding-left: 30px; border-radius: 6px;">
    <div class="" style="padding-top: 25px;padding-bottom: 25px;width: 47%">
        {!! Form::label('ficha_4', ' Titulo') !!}
        {!! Form::text('ficha_4', null, ['class' => 'form-control']) !!}
    </div>
    <div class="" style="padding-top: 25px;padding-bottom: 25px;width: 47%">
        {!! Form::label('link_4', 'Link') !!}
        {!! Form::text('link_4', null, ['class' => 'form-control']) !!}
    </div>
</div>


<hr class="mt-5 mb-3">

<h3 style="padding-top: 40px; padding-bottom: 20px">Frases dos Paineis</h3>

<div class="mb-3 col-12">
    {!! Form::label('frase_1', 'Frase do Segundo Painel') !!}
    {!! Form::textarea('frase_1', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="mb-3 col-12">
    {!! Form::label('link_video', 'URL de incorporação do Vídeo do Segundo Painel (exemplo: https://www.youtube.com/embed/_4kHxtiuML0)') !!}
    {!! Form::text('link_video', null, ['class' => 'form-control mb-5']) !!}
</div>

<div class="mb-0 col-12">
    {!! Form::label('frase_2', 'Frase do Terceiro Painel') !!}
    {!! Form::textarea('frase_2', null, ['class' => 'form-control editor-padrao']) !!}
</div>

<div class="" style="padding-top: 25px;padding-bottom: 25px;width: 47%">
    {!! Form::label('botao', 'Link para o Botão Saiba Mais') !!}
    {!! Form::text('botao', null, ['class' => 'form-control']) !!}
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('home.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>
