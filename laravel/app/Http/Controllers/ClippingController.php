<?php

namespace App\Http\Controllers;

use App\Models\Clipping;
use App\Models\ClippingGaleria;
use App\Models\ClippingImagem;
use App\Models\ClippingLink;
use App\Models\ClippingVideo;

class ClippingController extends Controller
{
    public function index()
    {

        $clippingGaleria = Clipping::where('tipo_id', 1)->ordenados()->get();
        $clippingLinks = Clipping::where('tipo_id', 2)->ordenados()->get();
        $clippingVideos = Clipping::where('tipo_id', 3)->ordenados()->get();
        

        $imagens = ClippingImagem::get();
        $links = ClippingLink::get();
        $videos = ClippingVideo::get();
        $linkVideo = "https://www.youtube.com/embed/";

        

        return view('site.clippings', compact('clippingGaleria', 'clippingVideos', 'clippingLinks', 'imagens', 'links', 'videos', 'linkVideo'));
    }
}
