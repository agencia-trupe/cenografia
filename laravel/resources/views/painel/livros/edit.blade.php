@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">Chamada do Livro</h2>
</legend>

{!! Form::model($livro, [
'route' => ['livros.update', $livro->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.livros.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection