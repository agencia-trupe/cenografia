@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<a href="{{ route('projetos.index') }}" title="Voltar para Projetos" class="btn btn-sm btn-primary mt-3 mb-3 col-2">
    &larr; Voltar para Projetos
</a>

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">LOGOS</h2>

    <a href="{{ route('projetos.logos.create', ['projeto' => $projeto->id]) }}" class="btn btn-success btn-sm">
        <i class="bi bi-plus-circle me-2 mb-1"></i>
        Adicionar Logo
    </a>
</legend>


@if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum logo cadastrado.</div>
@else
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover table-sortable" data-table="projetos_logos">
            <thead>
                <tr>
                    <th scope="col">Ordenar</th>
                    <th scope="col">Logo</th>
                    <th scope="col">Descrição - (SEO Alt)</th>
                    <th scope="col">Link</th>
                    <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
                </tr>
            </thead>

            <tbody>
                @foreach ($registros as $registro)
                <tr id="{{ $registro->id }}">
                    <td>
                        <a href="#" class="btn btn-dark btn-sm btn-move">
                            <i class="bi bi-arrows-move"></i>
                        </a>
                    </td>
                    <td>
                        <a href="{{$registro->link}}">
                            <img src="{{ asset('assets/img/logos/'.$registro->logo) }}" style="height: 90px; width: auto; max-width:100px;" alt="{{$registro->alt}}">
                        </a>
                    </td>
                    <td>
                        @if(isset($registro->alt))
                            <p>{{ $registro->alt }}</p>
                        @else
                            <p style="color:darkgray;"><i>Não foi adicionada uma descrição (Alt).</i></p>
                        @endif
                    </td>
                    <td>
                        @if(isset($registro->link))
                            <p>{{ $registro->link }}</p>
                        @else
                            <p style="color:darkgray;"><i>Não foi adicionado um link.</i></p>
                        @endif
                    </td>
                    <td class="crud-actions">
                        {!! Form::open([
                            'route' => ['projetos.logos.destroy', ['projeto' => $projeto->id, 'logo' => $registro->id]],
                            'method' => 'delete'
                            ]) !!}

                        <div class="btn-group btn-group-sm" role="group">
                            <a href="{{ route('projetos.logos.edit',  ['projeto' => $projeto->id, 'logo' => $registro->id]) }}" class="btn btn-primary btn-sm">
                                <i class="bi bi-pencil-fill me-2"></i>Editar
                            </a>

                            <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>
                        </div>

                        {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endif

@endsection