<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuemSomosTable extends Migration
{
    public function up()
    {
        Schema::create('quem_somos', function (Blueprint $table) {
            $table->id();
            $table->text('texto_1');
            $table->string('video');
            $table->text('titulo_2')->nullable();
            $table->text('texto_2');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('quem_somos');
    }
}
