@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0">QUEM SOMOS</h2>
</legend>

{!! Form::model($quem_somos, [
'route' => ['quem-somos.update', $quem_somos->id],
'method' => 'patch',
'files' => true])
!!}

@include('painel.quem-somos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection