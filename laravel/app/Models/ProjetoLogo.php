<?php

namespace App\Models;

use App\Helpers\CropImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjetoLogo extends Model
{
    use HasFactory;

    protected $table = 'projetos_logos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('logo', [
            'width'  => null,
            'height' => 90,
            'path'   => 'assets/img/logos/'
        ]);
    }

}
