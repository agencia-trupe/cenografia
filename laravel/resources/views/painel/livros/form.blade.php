@include('painel.layout.flash')

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($submitText == 'Alterar')
    @if($livro->imagem)
    <img src="{{ url('assets/img/livros/'.$livro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="mb-3">
    {!! Form::label('alt', 'Descrição da Imagem (Alt)', ['class' => 'form-label']) !!}
    @if($submitText == 'Alterar')
    {!! Form::text('alt', null, ['class' => 'form-control input-text']) !!}
    @else
    {!! Form::text('alt', null, ['class' => 'form-control input-text']) !!}
    @endif
</div>

<div class="mb-3">
    {!! Form::label('titulo', 'Título', ['class' => 'form-label']) !!}
    @if($submitText == 'Alterar')
    {!! Form::text('titulo', null, ['class' => 'form-control input-text']) !!}
    @else
    {!! Form::text('titulo', null, ['class' => 'form-control input-text']) !!}
    @endif
</div>

<div class="mb-3">
    {!! Form::label('subtitulo', 'Subtítulo', ['class' => 'form-label']) !!}
    @if($submitText == 'Alterar')
    {!! Form::textarea('subtitulo', null, ['class' => 'form-control editor-padrao']) !!}
    @else
    {!! Form::textarea('subtitulo', null, ['class' => 'form-control editor-padrao']) !!}
    @endif
</div>

<div class="mb-3">
    {!! Form::label('texto', 'Texto', ['class' => 'form-label']) !!}
    @if($submitText == 'Alterar')
    {!! Form::textarea('texto', null, ['class' => 'form-control editor-padrao']) !!}
    @else
    {!! Form::textarea('texto', null, ['class' => 'form-control editor-padrao']) !!}
    @endif
</div>

<div class="mb-3">
    {!! Form::label('botao', 'Texto do Botão', ['class' => 'form-label']) !!}
    @if($submitText == 'Alterar')
    {!! Form::text('botao', null, ['class' => 'form-control input-text']) !!}
    @else
    {!! Form::text('botao', null, ['class' => 'form-control input-text']) !!}
    @endif
</div>

<div class="mb-3">
    {!! Form::label('link_botao', 'Link do Botão', ['class' => 'form-label']) !!}
    @if($submitText == 'Alterar')
    {!! Form::text('link_botao', null, ['class' => 'form-control input-text']) !!}
    @else
    {!! Form::text('link_botao', null, ['class' => 'form-control input-text']) !!}
    @endif
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('livros.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>