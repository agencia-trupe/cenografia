<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClippingVideo extends Model
{
    protected $table = 'clipping_videos';

    protected $guarded = ['id'];

    public function scopeClipping($query, $id)
    {
        return $query->where('clipping_id', $id);
    }
}
