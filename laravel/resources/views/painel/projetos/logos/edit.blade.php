@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>LOGOS |</small> Editar Logo</h2>
</legend>

{!! Form::model($logo, [
'route' => ['projetos.logos.update', ['projeto' => $projeto->id, 'logo' => $logo->id]],
'method' => 'patch',
'files' => true])
!!}

@include('painel.projetos.logos.form', ['submitText' => 'Alterar'])

{!! Form::close() !!}

@endsection