<?php

namespace App\Http\Controllers;


use App\Models\Servico;
use App\Models\SobreServicos;
use Illuminate\Http\Request;

class ServicosController extends Controller
{
    public function index()
    {
        $servicos = Servico::ordenados()->get();
        $sobre = SobreServicos::first();

        return view('site.servicos', compact('servicos','sobre'));
    }

}
