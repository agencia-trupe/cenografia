@include('painel.layout.flash')

<div class="mb-3 col-12 col-md-12">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($submitText == 'Alterar')
    @if($banner->imagem)
    <img src="{{ url('assets/img/banners/'.$banner->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="mb-3">
    {!! Form::label('alt', 'Descrição da Imagem (Alt)', ['class' => 'form-label']) !!}
    @if($submitText == 'Alterar')
    {!! Form::text('alt', null, ['class' => 'form-control input-text']) !!}
    @else
    {!! Form::text('alt', null, ['class' => 'form-control input-text']) !!}
    @endif
</div>

<div class="mb-3">
    {!! Form::label('frase', 'Frase', ['class' => 'form-label']) !!}
    @if($submitText == 'Alterar')
    {!! Form::text('frase', null, ['class' => 'form-control input-text']) !!}
    @else
    {!! Form::text('frase', null, ['class' => 'form-control input-text']) !!}
    @endif
</div>

<div class="mb-3">
    {!! Form::label('link', 'Link', ['class' => 'form-label']) !!}
    @if($submitText == 'Alterar')
    {!! Form::text('link', null, ['class' => 'form-control input-text']) !!}
    @else
    {!! Form::text('link', null, ['class' => 'form-control input-text']) !!}
    @endif
</div>

<div class="d-flex align-items-center mt-4">
    {!! Form::submit($submitText, ['class' => 'btn btn-success me-1']) !!}

    <a href="{{ route('banners.index') }}" class="btn btn-secondary btn-voltar">Voltar</a>
</div>