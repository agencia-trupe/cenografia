<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\Sluggable;

class ProjetoCategoria extends Model
{
     use Sluggable;

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'titulo',
                'on_update' => true
            ]   
        ];           
    }

    protected $table = 'projetos_categorias';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    public function projetos()
    {
        return $this->hasMany('App\Models\Projeto', 'projetos_categoria_id')->ordenados();
    }
}
