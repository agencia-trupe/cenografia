@extends('painel.layout.template')

@section('content')

@include('painel.layout.flash')

<legend class="d-flex flex-row align-items-center justify-content-between mb-4">
    <h2 class="m-0">CLIPPINGS</h2>

    <a href="{{ route('clippings.create') }}" class="btn btn-success btn-sm">
        <i class="bi bi-plus-circle me-2 mb-1"></i>
        Adicionar Clipping
    </a>
</legend>

@if(!count($clippings))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<table class="table table-striped table-bordered table-hover table-sortable" data-table="clipping">
    <thead>
        <tr>
            <th>Ordenar</th>
            <th>Capa</th>
            <th>Título</th>
            <th>Gerenciar</th>
            <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
        </tr>
    </thead>

    <tbody>
        @foreach ($clippings as $clipping)
        <tr class="tr-row" id="{{ $clipping->id }}">
            <td>
                <a href="#" class="btn btn-dark btn-sm btn-move">
                    <i class="bi bi-arrows-move"></i>
                </a>
            </td>

            @if($clipping->tipo_id == 1)
            @foreach($capasImagens as $capa)
            @if($clipping->id == $capa->clipping_id)
            <td>
                <img src="{{ asset('assets/img/clippings/imagens/thumbs/'.$capa->imagem) }}" style="width: 100%; max-width:60px;" alt="">
            </td>
            @break
            @endif
            @endforeach

            @elseif($clipping->tipo_id == 2)
            @foreach($capasLinks as $capa)
            @if($clipping->id == $capa->clipping_id)
            <td>
                <img src="{{ asset('assets/img/clippings/link/'.$capa->capa) }}" style="width: 100%; max-width:60px;" alt="">
            </td>
            @break
            @endif
            @endforeach

            @elseif($clipping->tipo_id == 3)
            @foreach($capasVideos as $capa)
            @if($clipping->id == $capa->clipping_id)
            <td>
                <div class="video"><iframe style="width: 100%; max-width:100px; max-height:60px;" src="{{ $linkVideo.$capa->link_video }}"></iframe></div>
            </td>
            @break
            @endif
            @endforeach

            
            @endif

            <td>{{ $clipping->titulo }}</td>
            <!-- <td>{{ $clipping->ano }}</td> -->

            <td>
                @foreach($tipos as $tipo)
                @if($tipo->id == $clipping->tipo_id && $tipo->id == 1)
                <a href="{{ route('clippings.imagens.index', $clipping->id) }}" class="btn btn-secondary btn-gerenciar btn-sm" style="width:120px;">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>{{ $tipo->titulo }}
                </a>
                @endif
                @if($tipo->id == $clipping->tipo_id && $tipo->id == 2)
                <a href="{{ route('clippings.links.index', $clipping->id) }}" class="btn btn-secondary btn-gerenciar btn-sm" style="width:120px;">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>{{ $tipo->titulo }}
                </a>
                @endif
                @if($tipo->id == $clipping->tipo_id && $tipo->id == 3)
                <a href="{{ route('clippings.videos.index', $clipping->id) }}" class="btn btn-secondary btn-gerenciar btn-sm" style="width:120px;">
                    <span class="glyphicon glyphicon-picture" style="margin-right:10px;"></span>{{ $tipo->titulo }}
                </a>
                @endif
                @endforeach
            </td>

            <td class="crud-actions">
                {!! Form::open([
                'route' => ['clippings.destroy', $clipping->id],
                'method' => 'delete'
                ]) !!}

                <div class="btn-group btn-group-sm">
                    <a href="{{ route('clippings.edit', $clipping->id ) }}" class="btn btn-primary btn-sm pull-left">
                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                    </a>

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                </div>

                {!! Form::close() !!}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endif

@endsection