<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HomeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'frase_1' => 'required',
            'frase_2' => 'required',
            'ficha_1' => 'required',
            'link_1' => 'required',
            'ficha_2' => 'required',
            'link_2' => 'required',
            'ficha_3' => 'required',
            'link_3' => 'required',
            'ficha_4' => 'required',
            'link_4' => 'required',
            

        ];
    }
}
