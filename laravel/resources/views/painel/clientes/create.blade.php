@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>CLIENTES|</small> Adicionar Cliente</h2>
</legend>

{!! Form::open(['route' => 'clientes.store', 'files' => true]) !!}

@include('painel.clientes.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection