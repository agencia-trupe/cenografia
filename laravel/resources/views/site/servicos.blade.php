@extends('site.layout.template')

@section('content')

<main class="faixada">
    <img src="{{ asset('assets/img/layout/marca-cenografia-sustentavel.svg')}}" alt="logo" class="logo">
    <div class="texto">
        <h1>{!!$sobre->titulo!!}</h1>
        <p>{!!$sobre->texto!!}</p>
    </div>

</main>

<section>
    @foreach($servicos as $servico)
    <div class="servicos" id="{{$servico->slug}}">
        <div class="centers">
            <div class="imagembox">
                <img class="ball" src="{{ asset('assets/img/servicos/'.$servico->imagem)}}" alt="{{$servico->alt}}">
                <img class="mark" src="{{ asset('assets/img/layout/copa-arvore-avulsa-bolas.svg')}}" alt="copa arvore avulsa logo">
            </div>
            <div class="textbox">
                <h2>{!!$servico->titulo!!}</h2>
                <p>{!!$servico->texto!!}</p>
            </div>
        </div>
    </div>
    @endforeach
    
</section>


@endsection