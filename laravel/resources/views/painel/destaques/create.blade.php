@extends('painel.layout.template')

@section('content')

<legend class="mb-4">
    <h2 class="m-0"><small>DESTAQUES |</small> Adicionar Destaque</h2>
</legend>

{!! Form::open(['route' => 'destaques.store', 'files' => true]) !!}

@include('painel.destaques.form', ['submitText' => 'Adicionar'])

{!! Form::close() !!}

@endsection